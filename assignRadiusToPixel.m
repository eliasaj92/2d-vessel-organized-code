function assigned_Radius = assignRadiusToPixel(loc, Radius_Array)

assigned_Radius = ones(size(loc));

r_length = size(Radius_Array,2);

%I did this without the ranks
%to use the ranks, I have to search through index in VesselAxisGT
%then compare it to every Rank in Radius_Array

%TODO: implement binary search

        k = 1;
        while((k<=r_length))
            ind = loc>=Radius_Array(2,k);
            assigned_Radius(ind) = Radius_Array(1,k);
            k = k+1;
        end
end