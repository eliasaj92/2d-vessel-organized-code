function [temp_br, t_npoints, t_size, NBigSteps] = InitializeTemps(...
         VesselSkeleton, i, npoints, segment_size, NBigSteps)
   
     %function meant to initialize parameters for each new segment being
     %generate.
     %
     
     temp_br = VesselSkeleton(4,i);
     t_npoints = floor(npoints/1+temp_br);
     t_size = segment_size/temp_br;
     % conformity = sqrt((conformity^2+(1+temp_br))/(2+temp_br)); %make sub-branches straighter
     NBigSteps = NBigSteps -2; %reduce number of points per 
     %segment as to avoid
     if NBigSteps <5
         NBigSteps = 5; %need at least 5 points per segment
     end
     
end