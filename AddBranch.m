function [VesselSkeleton, VesselDir,Parent, polyform] = AddBranch(VesselSkeleton,VesselDir,...
         polyform, Parent, temp_br,branch1, branch_dir, temp_poly,k) 
     %code supposed to add branch to the whole structure
     
     
     rad_r1 = VesselSkeleton(3,(end))*ones(1,size(branch1,2)) +1; %this line and above
     %will help us keep track of the radius to be used when calculating the
     %intensity in the space
     %The Radius will depend on the radius rank of the point
    
     
     
     temp_p = VesselSkeleton(3,k-1)*ones(1,size( branch1,2));
     Parent = [Parent, temp_p];
     
     %update vessel with new branch
     branch_r1 = temp_br*ones(1,size(branch1,2)) + 1;
     temp_poly.id = size(VesselSkeleton,2)+1; %take index of starting point of new branch
     btemp = [branch1; rad_r1; branch_r1];
     VesselSkeleton = [VesselSkeleton, btemp];
     VesselDir = [VesselDir, branch_dir];
     if (temp_poly.addToArray == 1 )
        polyform = [polyform, temp_poly];
     end


end