function [d_ratio, loc_dr] = distanceFromVessel2DMesh(Vessel,...
    x, y, Radius_Array)

%This code calculates the distance of each pixel from the vessel
%This uses mesh grids for calculations of the minimum

[X,Y] = meshgrid(x,y);

c = size(Vessel,2);


d_ratio = inf*ones(size(X)); %supposed ratios to vessels
d2vessel = inf*ones(size(X)); %distance to vessel

loc_dv = ones(size(X)); %stores index of point on vessel
loc_dr = ones(size(X));


%The following is a fix to account for when the radius is too small
%compared to the distance between two spline points
%This arising problem makes it that there are small blobs of intensity
%across the vessel as the radius becomes too small

temp = [0;0]; %can't decide what value to assign to the first variable
temp = [temp, Vessel(1:2,1:end-1)];

diff = Vessel(1:2,:) - temp; %this is supposed to be the vectors 
%between consectuive points 
%note that an error occurs when we finish off a "main" vessel 

%the following lines are a fix for this

for i = 2:size(Vessel,2)
    if(Vessel(3,i-1)~= Vessel(3,i))
        if (Vessel(3,i-1) ~= (Vessel(5,i)) )
        
            ind2 = Vessel(5,i) == Vessel(3,:);
            temp_array = Vessel(:,ind2);
            temp_point = temp_array(:,end);
            diff(1:2,i) = Vessel(1:2,i) - temp_point(1:2,1);
        
        end
    end
end

point_dist = zeros(1,size(diff,2)); %supposed to be the distance between 
%two points in the spline
%in this case pointdist is between point i and i-1


for n = 1:size(diff,2)

    point_dist(1,n) = norm(diff(:,n));
end


%the following is the intesity calculations employing the fix

for i = 2:c %exclude first point
    Rad = assignRadiusToPixel(i, Radius_Array);
    mx = Vessel(1,i);
    my = Vessel(2,i);
    diff_X = X-mx;
    diff_Y = Y-my;
    tdist = sqrt((diff_X).*(diff_X) + (diff_Y).*(diff_Y));
    
    proj = (diff_X*diff(1,i)+diff_Y*diff(2,i)); %projecting onto 
    %the straightline to get the distance from the line is the main fix
    t_cos = (proj)./tdist/point_dist(1,i); %this value is NaN at branching points branching points duplicate for each segment
    t_sin = sqrt(1 - t_cos.*t_cos); 
    tdist_fixed = tdist.*t_sin; %fix the distance
    

    
    ind = proj<point_dist(1,i) & proj>0;% if projection is
    %bigger than the distance between the two points or negative 
    %then don't take into account the fix
    tdist(ind) = tdist_fixed(ind); %fix the distance where applicable

    tdist_r = tdist/Rad; %take the distance to radius ratio
    ind_r = tdist_r < d_ratio; %find the minimum of these ratios
    d_ratio(ind_r) = tdist_r(ind_r); 
    loc_dr(ind_r) = i;
end


end