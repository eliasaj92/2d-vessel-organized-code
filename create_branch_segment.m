function [VesselAxisGT, VesselDir, polyform] = create_branch_segment...
    (Vessel, NBigSteps, npoints, segment_size, u1, Points, conformity)

%xp and yp are the coordinates of the starting point

extra = 0.0; %this randomizes the length

%The next line is what determines the length of the vessel.
%For the time being, it was done by defining a rectangular space (in x and
%y)where the branch can extend, more or less.

step = segment_size/(2*NBigSteps);

xp = Points(1,1);
yp = Points(2,1);
m = size(Vessel,2);

xRange = sqrt(segment_size*segment_size*(0.5+0.1*rand(1,1)));
yRange = sqrt(segment_size*segment_size - xRange*xRange);
n = 1;
it =0; %check how many times we loop to avoid clash. If too many, stop looping and move on


step_reduction = 0; %keep track of how many degree did the step size decrease to

while (n <=NBigSteps & it<5)
    
    
    new_xp = step*u1(1,1) + xp + extra*randn*xRange; 
    new_yp = step*u1(1,2) + yp + extra*randn*yRange;
    
    
    
    if (m<2) %old algorithm did not include this condition, also need it for the base case
        xp = new_xp;
        yp = new_yp;
        u1 = getValidDirection(u1, conformity);
        Points=[Points,[xp;yp]];
        it=0; %reset it
    else
       clash = checkClash(1000*new_xp,1000*new_yp,1000*xp+0.0001,1000*yp+0.0001,1000*Vessel(1,1:end-1)',1000*Vessel(2,1:end-1)',...
           1000*Vessel(1,2:end)',1000*Vessel(2,2:end)');
         XY1 = [new_xp,new_yp, xp, yp];
         
         XY2 = [Vessel(1,1:end-1)',Vessel(2,1:end-1)',Vessel(1,2:end)',...
             Vessel(2,2:end)'];
         
         %clash = lineSegmentIntersect(XY1,XY2);
         
         
        if (sum(sum(clash)) > 2)
            n = n-1;
            
            theta = atan(u1(1,2)/u1(1,1));
            theta = theta + pi/100;
            it = it+1;
            u1 = [cos(theta),sin(theta)];
            %step = step/2;
            step_reduction = step_reduction+1;
        else

        xp = new_xp;
        yp = new_yp;
        Points=[Points,[xp;yp]];
        it = 0;
        u1 = getValidDirection(u1, conformity);
        end
    end
    n = n+1;
end    

if n>1
%instead of having this for all set of points, try to get
ratio = n/NBigSteps;%/(2^step_reduction);
npoints = ratio*npoints;

t = linspace(0,1,size(Points,2));
tt = linspace(0,1,npoints);


polyform = csapi(t, Points);
polyform.addToArray = 1;
VesselAxisGT = fnval(polyform,tt);

p1 = fnder(polyform,1);
VesselDir = fnval(p1,tt); %need to normalize this

else
    VesselAxisGT = [];
    VesselDir = [];
    polyform.addToArray = 0;
end