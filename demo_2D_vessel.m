NBigSteps = 8;
npoints = 20;
butterOrder = 6;
Radius = 4;
Noise = 0;
nx = 100; %nx and ny are the sampling
ny = 100;
dx = 2; %micron
dy = 2;
segment_size = 50;
BigAng = 10;
SmallAng = 40; 
conformity = 0.95;


close all;

[ Vessel, Radius_Array, polyform] =create_2D_vessel...
    (npoints, NBigSteps,...
    segment_size, Radius, BigAng, SmallAng, conformity, ...
    dx , dy );

R1 = drawVesselMesh(Vessel, Radius_Array,butterOrder, Noise, nx, ny, dx, dy );

[VesselStructure] = vesselToStructure(Vessel,...
    Radius_Array, polyform);