function [R, d_ratio] = drawVesselMesh(Vessel, Radius_Array,butterOrder, Noise, nx, ny, dx, dy )
%Vessel is the described centreline in the xy space as is.
%This script is supposed to give us different the drawing of the vessel
%when accounting for anisotropy
%nx smapling points in x; dx resolution in x
%Similary for ny and dy

xlocs = dx*(-nx/2:nx/2);
ylocs = dy*(-ny/2:ny/2);
%note that the space we're drawing our vessel in slightly bigger to try to
%contain the whole vessel

R = zeros(nx+1,ny+1); % zeros(size(xlocs1,2),size(ylocs,2));

[X,Y] = meshgrid(xlocs,ylocs);


[d_ratio, loc_dr] = distanceFromVessel2DMesh(Vessel, xlocs, ylocs, Radius_Array);

Radius = assignRadiusToPixel(loc_dr,d_ratio);

distance = d_ratio.*Radius;

sigma1 = 2;
sigma2 = 1;
% d_ratio = real(d_ratio);
%intensity = 1./(1+(d_ratio).^butterOrder); %fluorescence, butterworth
intensity = exp((-(d_ratio/(sigma1)).^2)./2) - 0.33*exp((-(d_ratio/(sigma2)).^2)./2); %Gaussian, can be improved by dicarding values  2 sigmas away
 

R = intensity + Noise*randn(size(intensity));

figure; imagesc(xlocs,ylocs,R);
end 