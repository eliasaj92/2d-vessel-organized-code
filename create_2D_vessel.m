function [VesselSkeleton, Radius_Array, polyform] = create_2D_vessel...
    (npoints, NBigSteps,segment_size, Radius, BigAng, SmallAng,...
    conformity, dx , dy)


%if dx=/dy then have anisotropy

%BigAng, SmallAng are the angles
%bifurcation respectively in degrees range: [0, 180]

M = npoints;  %number of samples in x direction
N = npoints;  %number of samples in y direction

xlocs = dx*(-N/2:N/2);
ylocs = dy*(-M/2:M/2);

xRange = xlocs(end)-xlocs(1);
yRange = ylocs(end)-ylocs(1);

[xp,yp, v] =  setStartSideAndDir2D(xlocs,ylocs); %chooses randomly one 
%of the 4 sides of the square surface to start from
Points = [xp;yp];

u1  = getValidDirection(v,conformity );
VesselDir(1,:) = u1';


%Generate first segment
[VesselSkeleton, VesselDir, polyform] = create_branch_segment...
    ([], NBigSteps, npoints, segment_size, u1, Points, conformity);

polyform.id = 1; %this is the ID of the vessel segment, to be used to 
%to save structures

v_length = size(VesselSkeleton, 2); %main branch length

Radius_Rank = ones(1, v_length); % radius rank is used to determine 
%radius to be used at point
Branch_rank = ones(1,v_length); %branch length is used to determine 
%length of branch at point

Parent = zeros(1,size(VesselSkeleton,2)); 
%reference the parent index represented by the Radius Rank of the
%parent. Names might need to be changed.

VesselSkeleton = [VesselSkeleton; Radius_Rank; Branch_rank];


npoints1 = floor(npoints/3);
x_r = floor(xRange/2);
y_r = floor(yRange/2);
conformity = sqrt((conformity^2+3)/4); %make sub-branches straighter
%function is random
temp_br = 1; %we use this value to determine what level of branching
%the algorithm is currently at. A value of 1 indicates the "main" vessel

%random walk on main branch to randomly determine branching points
%so far, only one branch per point
Radius_Array = [Radius; 1]; % array of the id of branch with its
%corresponding radius value
temp_Radius = Radius;%Radius to use for calcuations of daughter vessel radii

theta1 = SmallAng*pi/180; %convert to radian
theta2 = BigAng*pi/180; %convert to radians


u2 = [];

i = 1;


 while ((i<v_length+1) & (i <300) &(npoints1>=5))
     
     %temp variables to generate second branch of bifurcation 
     branch1 = []; %array for our first new branch points
     branch_dir1 = []; %this will be needed to get later branches
     rad_r1 = [];
     branch_r1 = [];
     
     %temp variables to generate second branch of bifurcation
     branch2 = []; %array for our second new  branch points
     branch_dir2 = [];
     rad_r2 = [];
     branch_r2 = [];
     
     
     [temp_br, t_npoints, t_size, NBigSteps] = InitializeTemps(...
         VesselSkeleton, i, npoints, segment_size, NBigSteps);
   
     
      if (i == v_length) | (VesselSkeleton(3,i) ~= VesselSkeleton(3,i+1))
        %above conditions is to find when the algorithm has moved into a
        %new branch, either when it creates one at the end of the current
        %vessel or just when it passes between ones it already generated


        temp_Radius = assignRadiusToPixel(i,Radius_Array);
        %TODO: somehow be able to get distance/ size of segment ratio from
        %fucntion below
        [R1, R2] = assignRadius(temp_Radius); % R1>=R2 gets news Radius values
        % for daughter vessels where R1>=R2;
              
          
          
     [u1, u2] = getBifurcationDirections(VesselDir(:,i)',theta1,...
              theta2); %want our new branch to be in another 
         %direction than where we are branching off
     Points = [VesselSkeleton(1,i);VesselSkeleton(2,i)];
         
    
     
    %first daughter branch     
    [branch1, branch_dir1, temp_poly1] =  create_branch_segment...
    (VesselSkeleton,NBigSteps, npoints, segment_size, u1, Points, conformity);
     if (size(branch1) ~= size([]))
         
         
      [VesselSkeleton, VesselDir,Parent, polyform] = AddBranch(VesselSkeleton,...
          VesselDir,polyform, Parent, temp_br,branch1, branch_dir1, temp_poly1,...
      i) ;

     
     R1 = [R1; v_length+1]; %takes index of branching point
     Radius_Array = [Radius_Array, R1];
     
     v_length = size(VesselSkeleton,2); 
     %if we want to remove sub-branching, we can just remove that line
     %the rest of the code will work well.


     
     end

    %first branch created now onto the second     
    [branch2, branch_dir2, temp_poly2] =  create_branch_segment...
    (VesselSkeleton,NBigSteps, npoints, segment_size/1.3, u2, Points, conformity) ;
    
    if (size(branch2) ~= size([]))
        
        
     
    [VesselSkeleton, VesselDir,Parent, polyform] = AddBranch(VesselSkeleton,...
          VesselDir,polyform, Parent, temp_br,branch2, branch_dir2, temp_poly2,...
      i) ;
     
     R2 = [R2; v_length+1]; %takes the index of starting point of
     %new branch
     Radius_Array = [Radius_Array, R2];
     
     Radius_Array =sortrows(Radius_Array',2)'; %sorts radius array 
     %according to the indeces where the corresponding branches start
       
     
     v_length = size(VesselSkeleton,2); %update v_length to include 
     %branching in branches
     
    end
     
        
      end 
     
     i = i+1;
     
 end 
 
temp_Rank = [];
 
 for j= 1:size(Radius_Array,2)
     temp_Rank = [temp_Rank, VesselSkeleton(3,Radius_Array(2,j))];
 end
 %Radius Array contains the radius and its correspoding rank
 %this is designed so that the radius lookup is based on rank, so making it
 %straightforward.
 Radius_Array = [Radius_Array; temp_Rank];
 VesselSkeleton = [VesselSkeleton; Parent];
  
%following code visualises the spline
 
hr=plot(VesselSkeleton(1,:),VesselSkeleton(2,:),'r.');
set(hr,'LineWidth',3);
hold on;


end