function [VesselStructure] = vesselToStructure(Vessel,...
    Radius_Array, polyform)
%This function is so that we save the Vessel in structure form so that we
%can reconstruct it later on.

id = 'id';
startingPoint = 'startingPoint';
endPoint = 'endPoint';
parent_id = 'parent_id';
daughter_id = 'daughter_id';
npoints = 'npoints';
Radius = 'Radius';
Rank = 'Rank';

%going to attemp to save the spline in here as well

form = 'form';
breaks = 'breaks';
coefs = 'coefs';
pieces = 'pieces';
order = 'order';
dim = 'dim';





VesselStructure = struct(id,[], startingPoint, {},endPoint,[] ,...
    parent_id , [], daughter_id ,[], npoints, [], Radius,[], Rank, [],...
    form, '', breaks, [], coefs, [], pieces, [], order, [], dim, []);

temp_branch_index = 0;

j = 1;

for i = 1:size(Vessel,2)
    
    if(Vessel(3,i) ~= temp_branch_index)
        temp_branch_index = Vessel(3,i);
        if (temp_branch_index>1)
            VesselStructure(temp_branch_index-1).endPoint =...
                {Vessel(1,i), Vessel(2,i)}; %need to decide on index
            VesselStructure(temp_branch_index-1).npoints = i-j; %get number of points along segment
            j = i;
        end
        VesselStructure(temp_branch_index).startingPoint = {Vessel(1,i), Vessel(2,i)};
        VesselStructure(temp_branch_index).id = Vessel(3,i);
        VesselStructure(temp_branch_index).Rank = Vessel(4,i);
        
        VesselStructure(temp_branch_index).parent_id = Vessel(5,i);
        VesselStructure(temp_branch_index).Radius = assignRadiusToPixel(i,Radius_Array);
        
        
    end
    if (i == size(Vessel,2))
            VesselStructure(temp_branch_index).endPoint =...
                {Vessel(1,i), Vessel(2,i)};
            VesselStructure(temp_branch_index).npoints = i-j+1;
            j = i+1;
    end
end

for k = 1:size(VesselStructure,2)
   for l =  1:size(VesselStructure,2)
    if(VesselStructure(l).parent_id == ...
            VesselStructure(k).id)
      VesselStructure(k).daughter_id = [  VesselStructure(k).daughter_id, l];
    end
   end
end


np = size(polyform,2);
spline_array = [];

for r = 1:size(polyform,2)
    p=1;
    while (p<=np & Radius_Array(2,r)>=polyform(p).id)
        temp_p = polyform(p);
        temp_p.id = Radius_Array(3,r);
        p = p+1;
    end
   
    spline_array = [spline_array, temp_p];
    
    
    VesselStructure(r).form = temp_p.form;
    VesselStructure(r).breaks = temp_p.breaks;
    VesselStructure(r).coefs = temp_p.coefs;
    VesselStructure(r).pieces = temp_p.pieces;
    VesselStructure(r).order = temp_p.order;
    VesselStructure(r).dim = temp_p.dim;
end




end