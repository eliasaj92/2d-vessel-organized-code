function [Vessel Radius_Array, polyform] = rebuildVessel(VesselStructure)

m = size(VesselStructure,2);
Vessel = [];
Radius_Array =[];

form = 'form';
breaks = 'breaks';
coefs = 'coefs';
pieces = 'pieces';
order = 'order';
dim = 'dim';
id = 'id';

polyform = struct(form, '', breaks, [], coefs, [], pieces, [], order, [], ...
    dim, [], id, []);

index = 1; %keeps track of the index along the vessel
%this is to be used in the Radius_Array to fill the second row 
%and the "id" field of the polyform structure

for i = 1:m
    
    tt = linspace(0,1,VesselStructure(i).npoints);
    V = fnval(VesselStructure(i),tt);
    Radius_Rank = VesselStructure(i).id*ones(1,size(V,2));
    Branch_Rank = VesselStructure(i).Rank*ones(1,size(V,2));
    parent = VesselStructure(i).parent_id*ones(1,size(V,2));
    V = [V;Radius_Rank;Branch_Rank;parent];
    Vessel = [Vessel, V];
    
    
    R = VesselStructure(i).Radius;
    R = [R; index;VesselStructure(i).id];
    Radius_Array = [Radius_Array, R];
    
    polyform(i).form = VesselStructure(i).form;
    polyform(i).breaks = VesselStructure(i).breaks;
    polyform(i).coefs = VesselStructure(i).coefs;
    polyform(i).pieces = VesselStructure(i).pieces;
    polyform(i).order = VesselStructure(i).order;
    polyform(i).dim = VesselStructure(i).dim;
    polyform(i).id = index;
    
    index = index+VesselStructure(i).npoints;
    
end


end