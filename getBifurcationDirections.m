function [u1, u2] = getBifurcationDirections(v, theta1, theta2)

v = v/norm(v); %normalize v.
c1 = cos(theta1*(0.95+0.1*rand(1,1)));

NoValidDirection = true;
while NoValidDirection 
    u1 = makerandunitdirvec(1);
    dp1 = v*u1';
    if (dp1<c1+0.05) & (dp1>c1-0.05)
        NoValidDirection = false;
    end
end

c2 = cos(theta2*(0.95+0.1*rand(1,1)));
c3 = cos(theta1+theta2*(0.95+0.1*rand(1,1)));


NoValidDirection = true;
while NoValidDirection 
    u2 = makerandunitdirvec(1);
    dp2 = v*u2';
    dp3 = u1*u2';
    if ((dp2<c2+0.1) & (dp2>c2-0.1)) & ((dp3<c3+0.08) & (dp3>c3-0.08))
        NoValidDirection = false;
    end
end

end



%the following works for 2D only

function u1=makerandunitdirvec(N)
v = randn(N,2);
u1 = bsxfun(@rdivide,v,sqrt(sum(v.^2,2)));
end
