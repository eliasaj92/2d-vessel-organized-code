function [R1, R2] = assignRadius(Radius)
%assign new radii based on Murray's law


R1 = (Radius^3*(0.5+0.05*rand(1,1)))^(1/3);
R2 = (Radius^3 - R1^3)^(1/3);

if R1<R2 %make sure R1>R2
    temp = R1;
    R1 = R2;
    R2 = temp;
end

end